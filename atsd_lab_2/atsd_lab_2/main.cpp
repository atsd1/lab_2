﻿#include <iostream>
#include <math.h>
#include <Windows.h>

int main()
{
    long long array[40000];


    long long a = 1664525;
    long long c = 1013904223;
    long long m = pow(2, 32);

    int intervals[250];
    int intervalFrequency[250];
    float staticProbability[250];

    long long x2 = 1;
    for (int i = 0; i < 40000; i++)
    {
        long long x = (a * x2 + c) % m;
        array[i] = x;
        x2 = x;
    }


    long long max = array[0];
    long long min = array[0];
    for (int i = 1; i < 40000; i++)
    {
        if (array[i] > max)
        {
            max = array[i];
        }
        if (array[i] < min)
        {
            min = array[i];
        }
    }

    for (int i = 0; i < 40000; i++)
    {
        array[i] = (array[i] * 249.99) / max;
    }

    for (int i = 0; i < 30; i++)
    {
        printf("Random value %d: %lld\n", i, array[i]);
    }

    for (int i = 0; i < 250; i++)
    {
        intervals[i] = i;
    }
    for (int j = 0; j < 250; j++)
    {
        int k = 0;
        for (int i = 0; i < 40000; i++)
        {
            if (array[i] == intervals[j])
            {
                k++;
            }
        }
        intervalFrequency[j] = k;
    }

    for (int j = 0; j < 250; j++)
    {
        printf("Interval frequency %d: %d\n", j, intervalFrequency[j]);
    }

    for (int j = 0; j < 250; j++)
    {
        staticProbability[j] = intervalFrequency[j] / 40000.;
        printf("Static probability of the interval %d: %f\n", j, staticProbability[j]);
    }

    float mathHope = 0;
    for (int i = 0; i < 250; i++)
    {
        mathHope += i * staticProbability[i];
    }
    printf("Math. hope: %f\n", mathHope);
    
    float dispersion = 0; 
    for (int j = 0; j < 250; j++)
    {
        dispersion += pow((array[j] - mathHope), 2) * staticProbability[j];
    }
    printf("Dispersion: %f\n", dispersion);
    
    float standartDev = sqrt(dispersion);
    printf("The standard deviation: %f\n", standartDev);
    
}